// Sample theme user settings data in JSON format.
//
// It contains information about the theme that user has
// chosen for his CV, especially its global editable parts
// such as color sheme and font sizes.

export default {
  themeID: 'razor',

  // color scheme can be organized by CV area ids
  // with always available defaults
  colorScheme: {
    default: {
      body: "#333",
      header: "#900",
    },
    sidebar: {
      background: "#900",
      header: "#fff",
      body: "#fff",
    },
    'userName': {
      header: "#fff",
      body: "#fff",
    },
  },
  fontSizes: {
    header: 22,
    body: 16,
  }
};
