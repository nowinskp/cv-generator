// Sample CV sections data in JSON format.
// It contains raw user data, such as all textual entries
// but also minimal amount of metadata related to component structure
// so that each entry can be properly rendered.

export default {
  // Each entry starts with unique random generated ID:
  vgzw7pne: {

    // Area defines section's desired position in a theme.
    // "main" is the default one, there can be custom ones such as "sidebar", etc.
    // This is more like a simplified solution as in a final one, this key should rather
    // store multiple entries containing position of this section in different themes.
    area: "main",

    // Type defines which component shall be used to display this section's content.
    // Matching is made using predefined config, in this case the one in sectionsConstants.js
    type: "description",

    // Meta is raw component data.
    // Its structure depends on chosen component type.
    meta: {
      title: "Summary",
      text: "Steve Nguyen, Ph.D., is a Leadership + Talent Development Advisor. A self-directed, enthusiastic, strategic problem-solver, he advises senior leaders on ways to make people and organizations more effective and is involved in developing and implementing key initiatives, training, and programs to create and sustain a high-performing organization.",
    },
  },
  trtvYhFd: {
    area: "main",
    type: "list",
    meta: {
      title: "Highlights",
      entries: [
        "12 years of experience in Leadership & Talent Development, Training/Learning & Development, and Organizational Development.",
        "3.5 years of leadership/talent development/organizational development consulting and corporate training experience in a Learning and Development organization.",
        "7.5 years of experience facilitating training for adults in a classroom environment.",
        "4.5 years of experience facilitating learning for adults in a virtual environment.",
        "Program Manager of a Corporate University/Academy.",
        "DISC Certified Practitioner.",
        "Myers-Briggs Type Indicator® (MBTI®) Certified.",
        "Lean Six Sigma Green Belt Certified.",
        "Prosci® Change Management Certified.",
      ]
    },
  },
  QKS2VmgP: {
    area: "userPhoto", // rather temporary solution to define static elements in a theme
    type: "userPhoto",
    meta: {
      url: "https://workstory.s3.amazonaws.com/assets/1527548/sn-photo-287x287_cv.png",
    }
  },
  VaTMocEO: {
    area: "userName",
    type: "userName",
    meta: {
      name: "Steve Nguyen, Ph. D",
      label: "Leadership + Talent Development Advisor",
    }
  },
  DUWJI41s: {
    area: "sidebar",
    type: "skillset",
    meta: {
      title: "Skills",
      entries: [
        {
          title: "Leadership Development",
          rating: 9,
        },
        {
          title: "Talent Management & Talent Development",
          rating: 9,
        },
        {
          title: "Organizational Development",
          rating: 9,
        },
        {
          title: "Training/Learning & Development",
          rating: 9,
        },
        {
          title: "Personnel Selection",
          rating: 7,
        },
      ],
    },
  },
  A1Xxgi9M: {
    area: "main",
    type: "history",
    meta: {
      title: "Work Experience",
      entries: [
        {
          timeRange: {
            from: {
              month: 4,
              year: 2014,
            },
            to: {
              month: 11,
              year: 2017,
            },
          },
          title: "Corporate Leadership & Talent Development Consultant",
          label: "MedSynergies",
          description: "MedSynergies, an Optum company, is a national leader in bringing hospitals and physician practices together to elevate patient care and improve financial performance.",
        },
        {
          timeRange: {
            from: {
              year: 2010,
            },
            to: {
              year: 2014,
            },
          },
          title: "College Professor",
          label: "University of Phoenix",
          description: "University of Phoenix is accredited by the Higher Learning Commission and is a member of the North Central Association, one of six regional accrediting bodies in the U.S.",
        },
        {
          timeRange: {
            from: {
              year: 2009,
            },
            to: {
              year: 2009,
            },
          },
          title: "Senior Diversity Trainer",
          label: "University of North Texas",
          description: "UNT is one of Texas' largest universities with 36,000+ students. It has a total endowment of $143.4 million (FY 2014).",
        },
      ],
    },
  },
  W3kPP021: {
    area: "main",
    type: "history",
    meta: {
      title: "Education",
      entries: [
        {
          timeRange: {
            from: {
              year: 1989,
            },
            to: {
              year: 1993,
            },
          },
          title: "Baylor University",
          label: "Bachelor of Arts - B.A. in Philosophy",
        },
        {
          timeRange: {
            from: {
              year: 1998,
            },
            to: {
              year: 2003,
            },
          },
          title: "Texas Woman's University",
          label: "Master of Arts - M.A. in Counseling Psychology",
        },
        {
          timeRange: {
            from: {
              year: 2008,
            },
            to: {
              year: 2013,
            },
          },
          title: "Capella University",
          label: "Doctor of Philosophy - Ph.D. in Industrial and Organizational Psychology",
        },
      ],
    },
  },
};
