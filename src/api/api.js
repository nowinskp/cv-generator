// A simple mock API to simulate real-life API connections

import sections from './mockData/sections';
import theme from './mockData/theme';

const DELAY_TIME = 1000;

// delay function to simulate server response time
const delay = val => new Promise(resolve => {
  setTimeout(resolve.bind(null, val), DELAY_TIME);
});

export const fetchSections = () => delay().then(() => sections);

export const fetchThemeSettings = () => delay().then(() => theme);
