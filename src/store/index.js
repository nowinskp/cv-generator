import configureStore, { history } from './configureStore';
import store from './store';

export {
  configureStore,
  history,
  store,
};
