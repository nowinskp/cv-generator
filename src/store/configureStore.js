import {createStore, compose, applyMiddleware} from 'redux';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import { routerMiddleware } from 'react-router-redux';
import rootReducer from '../modules/rootReducer';

export const history = createHistory();

// Store configuration for production environement
// ************************************************
function configureProductionStore(initialState) {
  const reactRouterMiddleware = routerMiddleware(history);
  const middlewares = [
    thunk,
    reactRouterMiddleware,
  ];

  return createStore(rootReducer, initialState, compose(
    applyMiddleware(...middlewares)
  ));
}

// Store configuration for development environement
// *************************************************
function configureDevelopmentStore(initialState) {
  const reactRouterMiddleware = routerMiddleware(history);
  const middlewares = [
    // Redux middleware that spits an error on you when you try to mutate your state either inside a dispatch or between dispatches.
    reduxImmutableStateInvariant(),
    thunk,
    reactRouterMiddleware,
  ];

  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // add support for Redux dev tools
  const store = createStore(rootReducer, initialState, composeEnhancers(
    applyMiddleware(...middlewares)
  ));

  if (module.hot) {
		// Enable Webpack hot module replacement for reducers
		module.hot.accept('../modules/rootReducer', () => {
			const nextReducer = require('../modules/rootReducer').default; // eslint-disable-line global-require
			store.replaceReducer(nextReducer);
		});
	}

  return store;
}

const configureStore = process.env.NODE_ENV === 'production' ? configureProductionStore : configureDevelopmentStore;

export default configureStore;
