import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { history, store } from './store/';
import Root from './modules/Root';

import './styles/index.scss';

render(
  <AppContainer>
    <Root store={store} history={history} />
  </AppContainer>,
  document.getElementById('app')
);

// setup for hot reloading, if available
if (module.hot) {
  module.hot.accept('./modules/Root', () => {
    const NewRoot = require('./modules/Root').default;
    render(
      <AppContainer>
        <NewRoot store={store} history={history} />
      </AppContainer>,
      document.getElementById('app')
    );
  });
}
