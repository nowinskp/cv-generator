import React from 'react';
import { NavLink } from "react-router-dom";

import './HomePage.scss';

const HomePage = () => (
  <div className="page-home">
    <ul className="main-menu">
      <li>
        <NavLink exact to="/cv">CV - online preview (Razor Theme)</NavLink>
      </li>
      <li>
        <NavLink exact to="/cv/edit">CV - edit mode</NavLink>
      </li>
    </ul>
  </div>
);

export default HomePage;
