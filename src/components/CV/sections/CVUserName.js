import React from 'react';
import PropTypes from 'prop-types';

import { CVText } from '../common/';

const CVUserName = ({
  colorScheme = {},
  fontSizes = {},
  meta,
}) => (
  <section className="cvSection userName">
    <h1 style={{ color: colorScheme.header }}>
      { meta.name }
    </h1>
    <CVText color={colorScheme.body} fontSize={fontSizes.body}>
      { meta.label }
    </CVText>
  </section>
);

CVUserName.propTypes = {
  colorScheme: PropTypes.object.isRequired,
  fontSizes: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
};

export default CVUserName;
