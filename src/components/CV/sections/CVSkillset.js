import React from 'react';
import PropTypes from 'prop-types';

import { CVHeader, CVSkill } from '../common/';
import './CVSkillset.scss';

const CVSkillset = ({
  colorScheme = {},
  fontSizes = {},
  meta,
}) => {
  return (
    <section className="cvSection skillset">
      <CVHeader color={colorScheme.header} fontSize={fontSizes.header}>
        { meta.title }
      </CVHeader>
      <ul className="skillsetList">
        { meta.entries.map((skill, i) => (
          <CVSkill
            key={'skill-' + i}
            color={colorScheme.body}
            title={skill.title}
            rating={skill.rating}
          />
        )) }
      </ul>
    </section>
  );
};

CVSkillset.propTypes = {
  colorScheme: PropTypes.object.isRequired,
  fontSizes: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
};

export default CVSkillset;
