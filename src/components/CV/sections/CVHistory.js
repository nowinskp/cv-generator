import React from 'react';
import PropTypes from 'prop-types';

import { CVHeader, CVDatedEntry } from '../common/';

import './CVHistory.scss';

const CVHistory = ({
  colorScheme = {},
  fontSizes = {},
  meta,
}) => (
  <section className="cvSection history">
    <CVHeader color={colorScheme.header} fontSize={fontSizes.header}>
      { meta.title }
    </CVHeader>
    <ul className="historyEntries">
      { meta.entries.map((entry, i) => (
        <CVDatedEntry
          key={'cvhistory-entry-' + i}
          as="li"
          colorScheme={colorScheme}
          fontSizes={fontSizes}
          entry={entry}
        />
      ))}
    </ul>
  </section>
);

CVHistory.propTypes = {
  colorScheme: PropTypes.object.isRequired,
  fontSizes: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
};

export default CVHistory;
