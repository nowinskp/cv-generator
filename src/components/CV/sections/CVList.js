import React from 'react';
import PropTypes from 'prop-types';

import { CVHeader, CVText } from '../common/';

import './CVList.scss';

const CVList = ({
  as = 'ul',
  colorScheme = {},
  fontSizes = {},
  meta,
}) => {
  const As = as;
  return (
    <section className="cvSection list">
      <CVHeader color={colorScheme.header} fontSize={fontSizes.header}>
        { meta.title }
      </CVHeader>
      <As className="listEntries">
        { meta.entries.map((item, i) => (
          <CVText
            key={'cvlist-element-' + i}
            as="li"
            color={colorScheme.body}
            fontSize={fontSizes.body}
          >
            { item }
          </CVText>
        ))}
      </As>
    </section>
  );
};


CVList.propTypes = {
  as: PropTypes.object,
  colorScheme: PropTypes.object.isRequired,
  fontSizes: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
};

export default CVList;
