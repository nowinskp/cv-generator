import React from 'react';
import PropTypes from 'prop-types';

import { CVHeader, CVText } from '../common/';

const CVDescription = ({
  colorScheme = {},
  fontSizes = {},
  meta,
}) => (
  <section className="cvSection description">
    <CVHeader color={colorScheme.header} fontSize={fontSizes.header}>
      { meta.title }
    </CVHeader>
    <CVText color={colorScheme.body} fontSize={fontSizes.body}>
      { meta.text }
    </CVText>
  </section>
);

CVDescription.propTypes = {
  colorScheme: PropTypes.object.isRequired,
  fontSizes: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
};

export default CVDescription;
