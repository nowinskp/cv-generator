import CVDescription from './CVDescription';
import CVHistory from './CVHistory';
import CVList from './CVList';
import CVSkillset from './CVSkillset';
import CVUserPhoto from './CVUserPhoto';
import CVUserName from './CVUserName';

export {
  CVDescription,
  CVHistory,
  CVList,
  CVSkillset,
  CVUserPhoto,
  CVUserName,
};
