import React from 'react';
import PropTypes from 'prop-types';

const CVUserPhoto = ({
  meta,
}) => (
  <div
    className="cvSection userPhoto"
    style={{
      backgroundImage: `url(${meta.url})`,
    }}
  />
);

CVUserPhoto.propTypes = {
  meta: PropTypes.object.isRequired,
};

export default CVUserPhoto;
