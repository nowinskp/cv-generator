import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const CVArea = ({
  as = 'div',
  areaID,
  backgroundColor = '#fff',
  children,
  className,
}) => {
  const As = as;
  return (
    <As
      className={classnames('cvArea', areaID, className)}
      style={{
        backgroundColor,
      }}
    >
      { children }
    </As>
  );
};

CVArea.propTypes = {
  areaID: PropTypes.string.isRequired,
  as: PropTypes.string,
  backgroundColor: PropTypes.string,
  children: PropTypes.node,
  className: PropTypes.string,
};

export default CVArea;
