import React from 'react';
import PropTypes from 'prop-types';

import { formatTimeRange } from '../../../utils/';

import CVText from './CVText';
import CVHeader from './CVHeader';

import './CVDatedEntry.scss';

const CVDatedEntry = ({
  as = 'div',
  colorScheme,
  entry,
}) => {
  const As = as;
  const {
    timeRange,
    title,
    label,
    description,
  } = entry;

  return (
    <As
      className="cvElement datedEntry"
    >
      <CVText color={colorScheme.body} className="timeRange">{ formatTimeRange(timeRange) }</CVText>
      <div className="entryDetails">
        <CVHeader as="h3" color={colorScheme.body} className="title">{ title }</CVHeader>
        <CVText color={colorScheme.body} className="label">{ label }</CVText>
        <CVText color={colorScheme.body} className="description">{ description }</CVText>
      </div>
    </As>
  );
};

CVDatedEntry.propTypes = {
  as: PropTypes.string,
  colorScheme: PropTypes.object,
  fontSizes: PropTypes.object,
  entry: PropTypes.object.isRequired,
};

export default CVDatedEntry;
