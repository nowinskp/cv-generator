import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import CVText from './CVText';
import './CVSkill.scss';

const CVSkill = ({
  as = 'div',
  className,
  color,
  fontSize,
  title,
  rating,
}) => {
  const As = as;
  const ratingPoints = [];
  for(let i = 1; i <= 10; i++) {
    ratingPoints.push(
      <li
        key={'rating-' + i}
        className={classnames('point', { filled: rating >= i })}
        style={{
          borderColor: color,
          backgroundColor: rating >= i ? color : 'transparent',
        }}
      />
    );
  }
  return (
    <As
      className={classnames("cvElement skill", className)}
    >
      <CVText color={color} fontSize={fontSize}>{ title }</CVText>
      <ul className="rating">{ ratingPoints }</ul>
    </As>
  );
};

CVSkill.propTypes = {
  as: PropTypes.string,
  className: PropTypes.string,
  color: PropTypes.string,
  fontSize: PropTypes.number,
  title: PropTypes.string.isRequired,
  rating: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]).isRequired,
};

export default CVSkill;
