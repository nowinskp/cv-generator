import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const CVText = ({
  as = 'div',
  className,
  color,
  fontSize,
  children,
}) => {
  const As = as;
  return (
    <As
      className={classnames("cvElement text", className)}
      style={{
        color,
        fontSize,
      }}
    >
      { children }
    </As>
  );
};

CVText.propTypes = {
  as: PropTypes.string,
  className: PropTypes.string,
  color: PropTypes.string,
  fontSize: PropTypes.number,
  children: PropTypes.node,
};

export default CVText;
