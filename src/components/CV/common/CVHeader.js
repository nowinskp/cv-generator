import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const CVHeader = ({
  as = 'h2',
  className,
  color,
  fontSize,
  children,
}) => {
  const As = as;
  return (
    <As
      className={classnames("cvElement header", className)}
      style={{
        color,
        fontSize,
      }}
    >
      { children }
    </As>
  );
};

CVHeader.propTypes = {
  as: PropTypes.string,
  className: PropTypes.string,
  color: PropTypes.string,
  fontSize: PropTypes.number,
  children: PropTypes.node,
};

export default CVHeader;
