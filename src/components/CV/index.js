import * as common from './common/';
import * as sections from './sections/';
import CVArea from './CVArea';

export {
  common,
  sections,
  CVArea,
};
