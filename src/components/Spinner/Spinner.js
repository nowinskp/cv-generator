import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './Spinner.scss';

const Spinner = ({
  size = 'medium',
  color = 'red',
  align,
}) => (
	<span
		className={classnames(
			'spinner',
			`size-${size}`,
			`color-${color}`,
			{
				[`align-${align}`]: align
			}
		)}
	/>
);

Spinner.propTypes = {
	align: PropTypes.string,
	color: PropTypes.string,
	size: PropTypes.string,
	tag: PropTypes.string
};

export default Spinner;
