import HomePage from './HomePage/HomePage';
import NotFoundPage from './NotFoundPage/NotFoundPage';
import Spinner from './Spinner/Spinner';

export {
  HomePage,
  NotFoundPage,
  Spinner,
};
