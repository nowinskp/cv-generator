import React from 'react';

const NotFoundPage = () => (
  <div className="page-not-found">
    Error #404 - page not found.
  </div>
);

export default NotFoundPage;
