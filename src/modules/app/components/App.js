import React from "react";
import PropTypes from "prop-types";
import { Route, Switch } from "react-router-dom";
import { hot } from "react-hot-loader";

import { CVPage, CVEditorPage } from '../../cv/pages/';
import { HomePage, NotFoundPage } from '../../../components/';

// This is a class-based component because the current
// version of hot reloading won't hot reload a stateless
// component at the top-level.

class App extends React.Component {
  render() {
    return (
      <div className="page-container">
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/cv" component={CVPage} />
          <Route path="/cv/edit" component={CVEditorPage} />
          <Route component={NotFoundPage} />
        </Switch>
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.element
};

export default hot(module)(App);
