import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import {reducer as cvReducer, constants as cvConstants} from './cv/';

export default combineReducers({
  routing: routerReducer,
  [cvConstants.NAME]: cvReducer,
});
