import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as sectionsActions from '../sectionsActions';
import CVSectionEditor from './CVSectionEditor';

class CVSectionEditorContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      meta: props.sectionObj.meta || {},
    };
    this.addNewListTypeItem = this.addNewListTypeItem.bind(this);
    this.addNewSkillsetTypeItem = this.addNewSkillsetTypeItem.bind(this);
    this.onMetaEntryFieldChange = this.onMetaEntryFieldChange.bind(this);
    this.onMetaEntryObjectChange = this.onMetaEntryObjectChange.bind(this);
    this.onMetaFieldChange = this.onMetaFieldChange.bind(this);
    this.onSaveSection = this.onSaveSection.bind(this);
    this.removeEntryItem = this.removeEntryItem.bind(this);
  }

  addNewListTypeItem() {
    this.setState({
      meta: {
        ...this.state.meta,
        entries: [...this.state.meta.entries, ''],
      },
    });
  }

  addNewSkillsetTypeItem() {
    this.setState({
      meta: {
        ...this.state.meta,
        entries: [...this.state.meta.entries, {
          title: '',
          rating: 0,
        }],
      },
    });
  }

  removeEntryItem(index) {
    let updatedArray = [...this.state.meta.entries];
    updatedArray.splice(index, 1);
    this.setState({
      meta: {
        ...this.state.meta,
        entries: updatedArray,
      },
    });
  }

  onMetaFieldChange(e) {
    this.setState({
      meta: {
        ...this.state.meta,
        [e.target.name]: e.target.value,
      },
    });
  }

  // For editing simple entries, e.g. list elements.
  // Input names should match array indexes.
  onMetaEntryFieldChange(e) {
    let updatedArray = [...this.state.meta.entries];
    updatedArray[e.target.name] = e.target.value;
    this.setState({
      meta: {
        ...this.state.meta,
        entries: updatedArray,
      },
    });
  }

  // For editing complex objects in entries arrays.
  // Inputs need to have index attr set.

  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  // FOR SOME REASON THIS IS FAULTY AND EFFECTS IN REDUX STATE MUTATION ERROR
  // THIS MUST GET FIXED ASAP!
  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  onMetaEntryObjectChange(e) {
    let updatedArray = [...this.state.meta.entries];
    updatedArray[e.target.dataset.index][e.target.name] = e.target.value;
    this.setState({
      meta: {
        ...this.state.meta,
        entries: [...updatedArray],
      },
    });
  }

  onSaveSection() {
    this.props.actions.updateSectionMeta(this.props.sectionId, this.state.meta);
    this.props.onCloseEditor();
  }

  render() {
    const {
      sectionObj
    } = this.props;
    const {
      meta
    } = this.state;
    return (
      <CVSectionEditor
        meta={meta}
        addNewListTypeItem={this.addNewListTypeItem}
        addNewSkillsetTypeItem={this.addNewSkillsetTypeItem}
        onMetaEntryFieldChange={this.onMetaEntryFieldChange}
        onMetaEntryObjectChange={this.onMetaEntryObjectChange}
        onMetaFieldChange={this.onMetaFieldChange}
        onSaveSection={this.onSaveSection}
        removeEntryItem={this.removeEntryItem}
        type={sectionObj.type}
      />
    );
  }
}

CVSectionEditorContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  onCloseEditor: PropTypes.func.isRequired,
  sectionObj: PropTypes.object.isRequired,
  sectionId: PropTypes.string.isRequired,
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(sectionsActions, dispatch),
});

export default connect(null, mapDispatchToProps)(CVSectionEditorContainer);
