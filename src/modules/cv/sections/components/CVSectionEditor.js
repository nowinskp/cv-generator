import React from 'react';
import PropTypes from 'prop-types';

import './CVSectionEditor.scss';

const CVSectionEditor = ({
  meta,
  addNewListTypeItem,
  addNewSkillsetTypeItem,
  onMetaEntryFieldChange,
  onMetaEntryObjectChange,
  onMetaFieldChange,
  onSaveSection,
  removeEntryItem,
  type,
}) => (
  <div className="cvSectionEditor">
    <div className="metaFields">
      <label>Section title:</label>
      <input name="title" value={meta.title} onChange={onMetaFieldChange} />
      {type === 'description' &&
        <div className="descriptionSectionFields">
          <label>Description:</label>
          <textarea name="text" value={meta.text} onChange={onMetaFieldChange} />
        </div>
      }
      {type === 'list' &&
        <div className="listSectionFields">
          <label>List items:</label>
          <ul>
          {meta.entries.map((entry, i) => (
            <li key={i}>
              <input name={i} value={meta.entries[i]} onChange={onMetaEntryFieldChange} />
              <div className="button removeEntryItem" onClick={removeEntryItem.bind(null, i)}>Remove</div>
            </li>
          ))}
            <li>
              <div className="button addNewItem" onClick={addNewListTypeItem}>Add new item</div>
            </li>
          </ul>
        </div>
      }
      {type === 'skillset' &&
        <div className="skillsetSectionFields">
          <label>Skills:</label>
          <ul>
          {meta.entries.map((entry, i) => (
            <li key={i}>
              <input name="title" data-index={i} value={meta.entries[i].title} onChange={onMetaEntryObjectChange} />
              <label className="ratingLabel">Rating</label>
              <select name="rating" data-index={i} value={meta.entries[i].rating} onChange={onMetaEntryObjectChange}>
                {[1,2,3,4,5,6,7,8,9,10].map(rating => <option key={'rating-'+rating} value={rating}>{rating}</option>)}
              </select>
              <div className="button removeEntryItem" onClick={removeEntryItem.bind(null, i)}>Remove</div>
            </li>
          ))}
            <li>
              <div className="button addNewItem" onClick={addNewSkillsetTypeItem}>Add new item</div>
            </li>
          </ul>
        </div>
      }
    </div>
    <div className="button saveSection" onClick={onSaveSection}>Save</div>
  </div>
);

CVSectionEditor.propTypes = {
  meta: PropTypes.object.isRequired,
  addNewListTypeItem: PropTypes.func.isRequired,
  addNewSkillsetTypeItem: PropTypes.func.isRequired,
  onSaveSection: PropTypes.func.isRequired,
  onMetaEntryFieldChange: PropTypes.func.isRequired,
  onMetaEntryObjectChange: PropTypes.func.isRequired,
  onMetaFieldChange: PropTypes.func.isRequired,
  removeEntryItem: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
};

export default CVSectionEditor;
