import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as sectionsActions from '../sectionsActions';
import { SECTION_TYPES } from '../sectionsConstants';
import CVSectionEditorWrapper from './CVSectionEditorWrapper';
import CVSectionEditorContainer from './CVSectionEditorContainer';

class CVSectionContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.sectionComponent = SECTION_TYPES[this.props.sectionObj.type] || null;
    this.state = {
      inEditMode: false,
    };
    this.onCloseSectionEditor = this.onCloseSectionEditor.bind(this);
    this.onStartSectionEditor = this.onStartSectionEditor.bind(this);
  }

  onStartSectionEditor() {
    // temporary fix to disable sections that shouldn't be editable atm
    if (
      this.props.isEditable
      && !this.state.inEditMode
      && !['userName', 'userPhoto'].includes(this.props.sectionObj.type)
    ) {
      this.setState({
        inEditMode: true,
      });
    }
  }

  onCloseSectionEditor() {
    this.setState({
      inEditMode: false,
    });
  }

  render() {
    const {
      isEditable,
      sectionObj: {
        area,
        meta,
        type,
      },
      themeSettings: {
        colorScheme,
        fontSizes,
      },
    } = this.props;
    const {
      inEditMode
    } = this.state;
    return (
      this.sectionComponent ?
        <CVSectionEditorWrapper
          // temp exclusion of userName/userPhoto
          isEditable={!['userName', 'userPhoto'].includes(type) && isEditable && !inEditMode}
          inEditMode={inEditMode}
          sectionType={type}
          onStartSectionEditor={this.onStartSectionEditor}
        >
          {inEditMode ?
            <CVSectionEditorContainer
              onCloseEditor={this.onCloseSectionEditor}
              sectionId={this.props.sectionId}
              sectionObj={this.props.sectionObj}
            />
            :
            <this.sectionComponent
              colorScheme={colorScheme[area] || colorScheme.default}
              fontSizes={fontSizes}
              meta={meta}
            />
          }
        </CVSectionEditorWrapper>
        : null
    );
  }
}

CVSectionContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  isEditable: PropTypes.bool,
  themeSettings: PropTypes.object.isRequired,
  sectionId: PropTypes.string.isRequired,
  sectionObj: PropTypes.object.isRequired,
};

const mapStateToProps = (state, ownProps) => {
  return {
    sectionObj: state.cv.sections.data[ownProps.sectionId],
  };
};

const mapDispatchToProps = dispatch => ({
  actions: {
    sections: bindActionCreators(sectionsActions, dispatch),
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(CVSectionContainer);
