import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './CVSectionEditorWrapper.scss';

const CVSectionEditorWrapper = ({
  children,
  inEditMode,
  isEditable,
  onStartSectionEditor,
  sectionType,
}) => (
  <div
    className={classnames("cvSectionEditorWrapper", sectionType, { editable: isEditable, inEditMode: inEditMode })}
    onClick={onStartSectionEditor}
  >
    { isEditable && <span className="clickToEditLabel">Click to edit</span> }
    { children }
  </div>
);

CVSectionEditorWrapper.propTypes = {
  children: PropTypes.node,
  inEditMode: PropTypes.bool,
  isEditable: PropTypes.bool,
  onStartSectionEditor: PropTypes.func,
  sectionType: PropTypes.string,
};

export default CVSectionEditorWrapper;
