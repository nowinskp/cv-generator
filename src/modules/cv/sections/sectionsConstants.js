import * as sections from '../../../components/CV/sections/';
export const NAME = 'sections';

export const SECTION_TYPES = {
  description: sections.CVDescription,
  history: sections.CVHistory,
  list: sections.CVList,
  userPhoto: sections.CVUserPhoto,
  userName: sections.CVUserName,
  skillset: sections.CVSkillset,
};
