import * as t from './sectionsActionTypes';
import initialState from './sectionsInitialState';

export default function sectionsReducer(state = initialState, action) {
  switch (action.type) {
    case t.REQUEST_SECTIONS:
      return {
        ...state,
        isLoaded: false,
        isFetching: true,
      };
    case t.REQUEST_SECTIONS_SUCCESS:
      return {
        ...state,
        isLoaded: true,
        isFetching: false,
        data: action.payload,
      };
    case t.UPDATE_SECTION_META:
      return {
        ...state,
        data: {
          ...state.data,
          [action.sectionId]: {
            ...state.data[action.sectionId],
            meta: action.sectionMeta,
          },
        },
      };
    default:
      return state;
  }
}
