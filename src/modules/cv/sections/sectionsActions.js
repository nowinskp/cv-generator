import * as t from './sectionsActionTypes';
import * as api from '../../../api/api';

export function fetchSections() {
  return dispatch => {
    dispatch({ type: t.REQUEST_SECTIONS });
    api.fetchSections().then(data => dispatch({
      type: t.REQUEST_SECTIONS_SUCCESS,
      payload: data,
    }));
  };
}

export function updateSectionMeta(sectionId, sectionMeta) {
  return {
    type: t.UPDATE_SECTION_META,
    sectionId,
    sectionMeta
  };
}
