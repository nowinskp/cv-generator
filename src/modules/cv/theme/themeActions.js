import * as t from './themeActionTypes';
import * as api from '../../../api/api';

export function fetchThemeSettings() {
  return dispatch => {
    dispatch({ type: t.REQUEST_THEME_SETTINGS });
    api.fetchThemeSettings().then(data => dispatch({
      type: t.REQUEST_THEME_SETTINGS_SUCCESS,
      payload: data,
    }));
  };
}
