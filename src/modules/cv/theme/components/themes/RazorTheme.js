import React from 'react';
import PropTypes from 'prop-types';

import { CVArea } from '../../../../../components/CV/';

import './RazorTheme.scss';

const RazorThemeComponent = ({
  sectionsInAreas,
  colorScheme,
}) => (
  <article className="cv-theme razorTheme">
    <CVArea
      areaID="header"
      as="header"
      backgroundColor={colorScheme.sidebar ? colorScheme.sidebar.background : undefined}
    >
      {sectionsInAreas.userPhoto}
      {sectionsInAreas.userName}
    </CVArea>
    <CVArea
      areaID="sidebar"
      as="aside"
      backgroundColor={colorScheme.sidebar ? colorScheme.sidebar.background : undefined}
    >
      {sectionsInAreas.sidebar}
    </CVArea>
    <CVArea areaID="main" as="main">
      {sectionsInAreas.main}
    </CVArea>
  </article>

);

RazorThemeComponent.propTypes = {
  sectionsInAreas: PropTypes.object.isRequired,
  colorScheme: PropTypes.object,
};

const RazorTheme = {
  config: {
    name: 'Razor Theme',
    areas: [
      'main',
      'sidebar',
      'userPhoto',
      'userName',
    ],
  },
  component: RazorThemeComponent,
};

export default RazorTheme;
