import RazorTheme from './RazorTheme';

export {
  RazorTheme
};

export const THEMES = {
  default: RazorTheme,
  razor: RazorTheme,
};
