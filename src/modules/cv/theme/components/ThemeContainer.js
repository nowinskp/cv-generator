import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import CVSectionContainer from '../../sections/components/CVSectionContainer';

import { THEMES } from './themes/';

class ThemeContainer extends React.PureComponent {

  constructor(props) {
    super(props);
    this.sectionsInAreas = {};
    this.theme = THEMES[props.settings.themeID] || THEMES['default'];
  }

  componentWillMount() {
    this.sectionsInAreas = this.sortSectionsByAreas(this.props.sections);
  }

  // go through all sections and assign them to available theme areas
  sortSectionsByAreas(sections) {
    const {isEditable, settings} = this.props;
    const sectionsInAreas = {};
    for (let sectionId in sections) {
      if (sections.hasOwnProperty(sectionId)) {
        const section = sections[sectionId];
        const area = this.determineTargetThemeArea(section);
        sectionsInAreas[area] = sectionsInAreas[area] || [];
        sectionsInAreas[area].push(
          <CVSectionContainer
            key={sectionId}
            sectionId={sectionId}
            isEditable={isEditable}
            themeSettings={settings}
          />
        );
      }
    }
    return sectionsInAreas;
  }

  determineTargetThemeArea(section) {
    const { area } = section;
    const availableAreas = this.theme.config.areas;
    return availableAreas.includes(area) ? area : 'main';
  }

  render() {
    return (
      <this.theme.component
        sectionsInAreas={this.sectionsInAreas}
        colorScheme={this.props.settings.colorScheme}
      />
    );
  }
}

ThemeContainer.propTypes = {
  isEditable: PropTypes.bool,
  settings: PropTypes.object.isRequired,
  sections: PropTypes.object.isRequired,
};


const mapStateToProps = state => {
  const { sections, theme } = state.cv;
  return {
    sections: sections.data,
    settings: theme.settings,
  };
};

export default connect(mapStateToProps)(ThemeContainer);
