import * as t from './themeActionTypes';
import initialState from './themeInitialState';

export default function themeReducer(state = initialState, action) {
  switch (action.type) {
    case t.REQUEST_THEME_SETTINGS:
      return {
        ...state,
        isLoaded: false,
        isFetching: true,
      };
    case t.REQUEST_THEME_SETTINGS_SUCCESS:
      return {
        ...state,
        isLoaded: true,
        isFetching: false,
        settings: action.payload,
      };
    default:
      return state;
  }
}
