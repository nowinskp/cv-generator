import { combineReducers } from 'redux';

import {reducer as themeReducer, constants as themeConstants} from './theme/';
import {reducer as sectionsReducer, constants as sectionsConstants} from './sections/';

export default combineReducers({
	[themeConstants.NAME]: themeReducer,
	[sectionsConstants.NAME]: sectionsReducer,
});
