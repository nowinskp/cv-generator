import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { actions as sectionsActions } from '../sections/';
import { actions as themeActions } from '../theme/';

import CVDisplay from './CVDisplay';

class CVDisplayContainer extends React.PureComponent {

  componentDidMount() {
    this.props.actions.sections.fetchSections();
    this.props.actions.theme.fetchThemeSettings();
  }

  render() {
    const { isEditable, isReady } = this.props;
    return (
      <CVDisplay
        isEditable={isEditable}
        isReady={isReady}
      />
    );
  }
}

CVDisplayContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  isEditable: PropTypes.bool,
  isReady: PropTypes.bool,
};

const mapStateToProps = state => {
  const { sections, theme } = state.cv;
  return {
    isReady: sections.isLoaded && theme.isLoaded,
  };
};

const mapDispatchToProps = dispatch => ({
  actions: {
    sections: bindActionCreators(sectionsActions, dispatch),
    theme: bindActionCreators(themeActions, dispatch),
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(CVDisplayContainer);
