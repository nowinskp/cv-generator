import CVDisplayContainer from "./CVDisplayContainer";
import CVDisplay from "./CVDisplay";

export {
  CVDisplay,
  CVDisplayContainer,
};
