import React from 'react';
import PropTypes from 'prop-types';

import { Spinner } from '../../../components/';
import ThemeContainer from '../theme/components/ThemeContainer';
import './CVDisplay.scss';

const CVDisplay = ({
  isEditable,
  isReady,
}) => (
  <div className="cvDisplay">
    { isReady ? <ThemeContainer isEditable={isEditable} /> :  <Spinner align="center" /> }
  </div>
);

CVDisplay.propTypes = {
  isEditable: PropTypes.bool,
  isReady: PropTypes.bool,
};

export default CVDisplay;
