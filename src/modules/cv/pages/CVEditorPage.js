import React from 'react';

import { CVDisplayContainer } from '../components/';

class CVEditorPage extends React.PureComponent {
  render() {
    return (
      <div className="cvEditorPage">
        <CVDisplayContainer isEditable />
      </div>
    );
  }
}

export default CVEditorPage;
