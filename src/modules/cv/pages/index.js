import CVEditorPage from "./CVEditorPage";
import CVPage from "./CVPage";

export {
  CVEditorPage,
  CVPage,
};
