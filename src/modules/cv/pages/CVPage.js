import React from 'react';

import { CVDisplayContainer } from '../components/';

class CVPage extends React.PureComponent {
  render() {
    return (
      <div className="cvPage">
        <CVDisplayContainer />
      </div>
    );
  }
}

export default CVPage;
