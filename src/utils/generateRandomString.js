const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

export default (length) => {
	if (!length) {
		return '';
	}
	let result = '';
	let array;

	if ('Uint8Array' in self && 'crypto' in self && length <= 65536) {
		array = new Uint8Array(length);
		self.crypto.getRandomValues(array);
	} else {
		array = new Array(length);

		for (let i = 0; i < length; i++) {
			array[i] = Math.floor(Math.random() * chars.length);
		}
	}

	for (let i = 0; i < length; i++) {
		result += chars.charAt(array[i] % chars.length);
	}

	return result;
};
