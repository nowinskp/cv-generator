import moment from 'moment';
// timeRange format:
// {
//   from: {
//     month: 4, // zeroindexed
//     year: 2014,
//   },
//   to: {
//     ...
//   },
// }

export default (timeRange) => {
  let timeString = '';

  if (timeRange.from) {
    if (!timeRange.to) {
      timeString = 'Since ';
    }
    if (timeRange.from.month) {
      timeString += moment().month(timeRange.from.month).format('MMM');
    }
    if (timeRange.from.year) {
      timeString += ' ' + timeRange.from.year;
    }
  }

  if (timeRange.to) {
    if (!timeRange.from) {
      timeString = 'Until ';
    } else {
      timeString += ' - ';
    }
    if (timeRange.to.month) {
      timeString += moment().month(timeRange.to.month).format('MMM');
    }
    if (timeRange.to.year) {
      timeString += ' ' + timeRange.to.year;
    }
  }

  return timeString;
};
