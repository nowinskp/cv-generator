# CV Generator

## Task

Prepare an interactive CV with editable elements.

## Approach

For the purpose of this task, the general idea is to prepare a website that will display CV data parsed from JSON fetched from a mock API.

### Assumptions

1. Information contained in CV JSON config file should be fully sufficient to display the CV in a predictable manner.
2. Configuration related strictly to theme could be fetched from a separate JSON.
3. CV JSON should contain both CV's data and information about the position of said data in the theme (layout).
4. Application knows how to parse the JSON and use its contained meta and layout data.
5. Application should be maintainable and easily extendable.
6. Application should generate the content dynamically.
7. Application should allow editing of said dynamically generated content.

## Themes

Each theme consists of areas that act as containers for CV sections.

Each theme must have at least a 'main' section which is used as default fallback.

## Example CV's Sections JSON definition

See [/api/mockData/sections.js](../src/api/mockData/sections.js)

## Example theme settings JSON definition

See [/api/mockData/theme.js](../src/api/mockData/theme.js)
