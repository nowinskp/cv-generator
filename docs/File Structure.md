# Folder & file structure

"[RS]" tag in descriptions marks files/folders that are specific to React Slingshot boilerplate and were left mostly unaltered.

```bash
.
├── .editorconfig             # Configures editor rules
├── .eslintrc                 # Configures eslint rules
├── .gitignore                # Tells git which files to ignore
├── .istanbul.yml             # [RS] Configure istanbul code coverage
├── .npmrc                    # Configures npm to save exact by default
├── README.md                 # [RS] Original React Slingshot README file
├── dist                      # Folder where the build script places the built app - for use in production.
├── package.json              # Package configuration. The list of 3rd party libraries and utilities
├── src                       # Source code
│   ├── assets                # Static files such as images, fonts, svgs, etc.
│   ├── components            # Core React components, not connected to Redux
│   ├── constants             # Universal application constants
│   ├── modules               # Compositions of React components and their logic, separated by responsibility, connected to Redux
│   │   ├── app               # App's base component with general logic
│   │   ├── Root.js           # App's entry point, containing startup logic
│   │   └── rootReducer.js    # Core Redux reducer that combines all other reducers from across the app
│   ├── store                 # Redux store & its configuration
│   ├── styles                # Core SASS CSS Styles, mostly used across the whole app
│   ├── utils                 # Plain old JS objects (POJOs). Pure logic. No framework specific code here.
│   ├── index.ejs             # Template for homepage
│   └── index.js              # App mounting point with HMR setup
├── tools                     # [RS] Node scripts that run build related tools
│   └── analyzeBundle.js      # [RS] Analyzes the webpack bu1ndle
│   ├── assetsTransformer.js  # [RS] Fix for jest handling static assets like imported images
│   ├── setup                 # [RS] Scripts for setting up a new project using React Slingshot
│   │   ├── setup.js          # [RS] Configure project set up
│   │   ├── setupMessage.js   # [RS] Display message when beginning set up
│   │   └── setupPrompts.js   # [RS] Configure prompts for set up
│   ├── build.js              # [RS] Runs the production build
│   ├── chalkConfig.js        # [RS] Centralized configuration for chalk (adds color to console statements)
│   ├── distServer.js         # [RS] Starts webserver and opens final built app that's in dist in your default browser
│   ├── nodeVersionCheck.js   # [RS] Confirm supported Node version is installed
│   ├── removeDemo.js         # [RS] Remove demo app
│   ├── srcServer.js          # [RS] Starts dev webserver with hot reloading and opens your app in your default browser
│   ├── startMessage.js       # [RS] Display message when development build starts
│   ├── testCi.js             # [RS] Configure Jest to run on a CI server
├── webpack.config.dev.js     # [RS] Configures webpack for development builds
└── webpack.config.prod.js    # [RS] Configures webpack for production builds
```